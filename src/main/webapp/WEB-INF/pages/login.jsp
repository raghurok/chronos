<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Chronos Login</title>


<link href="/chronos/resources/css/main.css" rel="stylesheet" type="text/css" media="screen">
</head>

<body>
<div id="container">
    <form:form id="loginForm" method="POST" action="login.html" modelAttribute="login">    
        <label for="username">Username:</label>
        <form:input type="text" tabindex="1" id="username" path="username"/>
       
        <label for="password">Password:</label>
        <form:input type="password" tabindex="2" id="password" path="password" />
                
            
        <div id="lower"> 
            <input type="submit" value="Login">
        </div>
        
    </form:form>
</div>
</body>
</body>
</html>