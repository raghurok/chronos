package com.chronos.dao;

import com.chronos.entity.ProjectDetails;

public interface ProjectDetailsDAO {
	public abstract ProjectDetails getProjectDetailsByProjectId(String projectId);
}
