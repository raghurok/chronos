package com.chronos.dao;

import com.chronos.entity.Role;

public interface RoleDAO {
	public abstract Role getRoleDetailsByRoleId(Long id);
}
