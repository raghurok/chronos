package com.chronos.dao;

import com.chronos.entity.UserXProject;
import com.chronos.entity.UserXProjectCpk;

public interface UserXProjectDAO {
	public abstract UserXProject getDetailsByUserIdProjectIdCPkey(UserXProjectCpk userXprojectId);
}
