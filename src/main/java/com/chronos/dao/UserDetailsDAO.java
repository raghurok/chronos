package com.chronos.dao;

import java.util.List;

import com.chronos.entity.UserDetails;

public interface UserDetailsDAO {
	
	public UserDetails getUserDetailsByUserId(String userId);
	
}
