package com.chronos.dao;

import com.chronos.entity.TaskXProject;

public interface TaskXProjectDAO {
	public abstract TaskXProject getTaskProjectDetailsByTaskId(Long taskId);
}
