package com.chronos.dao;

import com.chronos.entity.Team;

public interface TeamDAO {
	public abstract Team getTeamDetailsByTeamId(Long id);
}
