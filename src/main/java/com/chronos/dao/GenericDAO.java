package com.chronos.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, ID extends Serializable> {
	
	public abstract boolean insert(T entity);
	
	public abstract boolean update(T entity);
	
	public abstract boolean delete(T entity);
	
	public abstract T findByPk(ID id);

	public abstract List<T> getAllDetails();
	
	public abstract boolean insertAll(List<T> insertList);
	
	public abstract boolean updateAll(List<T> updateList);
	
	public abstract boolean deleteAll(List<T> deleteList);
	
	public abstract List<T> findByExample(T exampleEntity);
}
