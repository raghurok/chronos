package com.chronos.dao.impl;

import org.hibernate.Session;

import com.chronos.dao.TaskXProjectDAO;
import com.chronos.entity.TaskXProject;
import com.chronos.entity.Team;

public class TaskXProjectDAOImpl extends GenericDAOImpl<TaskXProject, Long> implements
		TaskXProjectDAO {

	@Override
	public TaskXProject getTaskProjectDetailsByTaskId(Long taskId) {
			Session session = getSession();
			TaskXProject taskXProject = null;
			try {
				session = checkSessionTxnInitiation(session);
				taskXProject = (TaskXProject) session.get(TaskXProject.class, taskId);

				if (getOverrideSession() == false) {
					session.getTransaction().commit();
				}
			} catch (RuntimeException ex) {
				ex.printStackTrace();
				if (session != null)
					session.getTransaction().rollback();
				throw ex;
			}
			return taskXProject;
	}
}

