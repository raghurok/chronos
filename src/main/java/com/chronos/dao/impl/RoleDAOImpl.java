package com.chronos.dao.impl;

import org.hibernate.Session;

import com.chronos.dao.RoleDAO;
import com.chronos.entity.Role;

public class RoleDAOImpl extends GenericDAOImpl<Role, Long> implements RoleDAO{

	@Override
	public Role getRoleDetailsByRoleId(Long id) {
		Session session = getSession();
		Role role = null;
		try {
			session = checkSessionTxnInitiation(session);
			role = (Role) session.get(Role.class, id);
			
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return role;
	}

}
