package com.chronos.dao.impl;

import org.hibernate.Session;

import com.chronos.dao.ProjectDetailsDAO;
import com.chronos.entity.ProjectDetails;

public class ProjectDetailsDAOImpl extends
		GenericDAOImpl<ProjectDetails, String> implements ProjectDetailsDAO {

	@Override
	public ProjectDetails getProjectDetailsByProjectId(String projectId) {
		Session session = getSession();
		ProjectDetails projectDetails = null;
		try {
			session = checkSessionTxnInitiation(session);
			projectDetails = (ProjectDetails) session.get(ProjectDetails.class, projectId);
			
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return projectDetails;
	}

}
