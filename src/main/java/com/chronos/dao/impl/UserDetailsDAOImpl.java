package com.chronos.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Example;

import com.chronos.dao.UserDetailsDAO;
import com.chronos.entity.UserDetails;

public class UserDetailsDAOImpl extends GenericDAOImpl<UserDetails, String>
		implements UserDetailsDAO {

	@Override
	public UserDetails getUserDetailsByUserId(String userId) {
		Session session = getSession();
		UserDetails userDetails = null;
		try {
			session = checkSessionTxnInitiation(session);
			userDetails = (UserDetails) session.get(UserDetails.class, userId);
			
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return userDetails;
	}
/*
	@Override
	public boolean insert(UserDetails insertRow) {
		Session session = getSession();
		
		try{
			session = checkSessionTxnInitiation(session);
			session.save(insertRow);
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return true;
	}

	@Override
	public boolean update(UserDetails updateRow) {
		Session session = getSession();
		
		try{
			session = checkSessionTxnInitiation(session);
			session.update(updateRow);
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		
		return true;
	}

	@Override
	public boolean delete(UserDetails deleteRow) {
		Session session = getSession();
		
		try{
			session = checkSessionTxnInitiation(session);
			session.delete(deleteRow);
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		
		return true;
	}

	@Override
	public UserDetails findByPk(String id) {
		return getUserDetailsByUserId(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserDetails> getAllDetails() {
		Session session = getSession();
		List<UserDetails> allUserDetails = null;;
		try{
			session = checkSessionTxnInitiation(session);
			allUserDetails = (List<UserDetails>) session.createCriteria(UserDetails.class).list();
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		
		return allUserDetails;
	}

	@Override
	public boolean insertAll(List<UserDetails> insertList) {
		Session session = getSession();
		try{
			session = checkSessionTxnInitiation(session);
			for(UserDetails userDetails: insertList){
				session.save(userDetails);
			}
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		
		return true;
	}

	@Override
	public boolean updateAll(List<UserDetails> updateList) {
		Session session = getSession();
		try{
			session = checkSessionTxnInitiation(session);
			for(UserDetails userDetails: updateList){
				session.update(userDetails);
			}
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		
		return true;
	}

	@Override
	public boolean deleteAll(List<UserDetails> deleteList) {
		Session session = getSession();
		try{
			session = checkSessionTxnInitiation(session);
			for(UserDetails userDetails: deleteList){
				session.delete(userDetails);
			}
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		
		return true;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserDetails> findByExample(UserDetails exampleEntity) {
		Session session = getSession();
		List<UserDetails> allUserDetails = null;;
		try{
			session = checkSessionTxnInitiation(session);
			Example example = Example.create(exampleEntity);
			allUserDetails = (List<UserDetails>) session.createCriteria(UserDetails.class).add(example);
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		
		return allUserDetails;
	}*/
}
