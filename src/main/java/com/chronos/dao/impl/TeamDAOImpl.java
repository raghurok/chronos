package com.chronos.dao.impl;

import org.hibernate.Session;

import com.chronos.dao.TeamDAO;
import com.chronos.entity.Team;

public class TeamDAOImpl extends GenericDAOImpl<Team, Long> implements TeamDAO{
	@Override
	public Team getTeamDetailsByTeamId(Long id) {
		Session session = getSession();
		Team team = null;
		try {
			session = checkSessionTxnInitiation(session);
			team = (Team) session.get(Team.class, id);

			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return team;
	}

}
