package com.chronos.dao.impl;

import org.hibernate.Session;

import com.chronos.dao.UserXProjectDAO;
import com.chronos.entity.UserDetails;
import com.chronos.entity.UserXProject;
import com.chronos.entity.UserXProjectCpk;

public class UserXProjectDAOImpl extends
		GenericDAOImpl<UserXProject, UserXProjectCpk> implements UserXProjectDAO {

	@Override
	public UserXProject getDetailsByUserIdProjectIdCPkey(UserXProjectCpk userXprojectId) {
		Session session = getSession();
		UserXProject userXProject = null;
		try {
			session = checkSessionTxnInitiation(session);
			userXProject = (UserXProject) session.get(UserXProject.class, userXprojectId);
			
			if (getOverrideSession() == false){
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return userXProject;
	}

}
