package com.chronos.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Example;

import com.chronos.dao.GenericDAO;
import com.chronos.util.HibernateSessionManager;

public abstract class GenericDAOImpl<T, ID extends Serializable> implements
		GenericDAO<T, ID> {

	private Session session;
	private boolean overrideSession = false;
	private Class<?> Tclass = (Class<?>) ((ParameterizedType) getClass()
			.getGenericSuperclass()).getActualTypeArguments()[0];

	public Session checkSessionTxnInitiation(Session session)
			throws HibernateException {
		if (session == null && getOverrideSession() == false) {
			session = HibernateSessionManager.getSessionFactory()
					.getCurrentSession();
			session.beginTransaction();
		}
		return session;
	}

	public void setOverrideSession(boolean overrideSession) {
		this.overrideSession = overrideSession;
	}

	public boolean getOverrideSession() {
		return overrideSession;
	}

	public void setSession(Session session) {
		this.session = session;
		overrideSession = true;
	}

	public Session getSession() {
		return session;
	}

	public void beginTransaction() {
		session.beginTransaction();
	}

	public boolean commitTransaction() {
		session.getTransaction().commit();
		return true;
	}

	public boolean rollbackTransaction() {
		session.getTransaction().rollback();
		return true;
	}

	@Override
	public boolean insert(T entity) {
		Session session = getSession();

		try {
			session = checkSessionTxnInitiation(session);
			session.save(entity);
			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return true;
	}

	@Override
	public boolean update(T entity) {
		Session session = getSession();

		try {
			session = checkSessionTxnInitiation(session);
			session.update(entity);
			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}

		return true;
	}

	@Override
	public boolean delete(T entity) {
		Session session = getSession();
		try {
			session = checkSessionTxnInitiation(session);
			session.delete(entity);
			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T findByPk(ID id) {
		Session session = getSession();
		T entity = null;
		
		try {
			session = checkSessionTxnInitiation(session);
			entity = (T) session.get(Tclass, id);

			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAllDetails() {
		Session session = getSession();
		List<T> allDetailsList = null;
		;
		try {
			session = checkSessionTxnInitiation(session);
			allDetailsList = (List<T>) session.createCriteria(
					Tclass).list();
			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}

		return allDetailsList;
	}

	@Override
	public boolean insertAll(List<T> insertList) {
		Session session = getSession();
		try {
			session = checkSessionTxnInitiation(session);
			for (T insertRow : insertList) {
				session.save(insertRow);
			}
			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}

		return true;
	}

	@Override
	public boolean updateAll(List<T> updateList) {
		Session session = getSession();
		try {
			session = checkSessionTxnInitiation(session);
			for (T updateRow : updateList) {
				session.update(updateRow);
			}
			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}

		return true;
	}

	@Override
	public boolean deleteAll(List<T> deleteList) {
		Session session = getSession();
		try {
			session = checkSessionTxnInitiation(session);
			for (T deleteRow : deleteList) {
				session.delete(deleteRow);
			}
			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByExample(T exampleEntity) {
		Session session = getSession();
		List<T> entityList = null;
		;
		try {
			session = checkSessionTxnInitiation(session);
			Example example = Example.create(exampleEntity);
			entityList = (List<T>) session
					.createCriteria(Tclass).add(example).list();
			if (getOverrideSession() == false) {
				session.getTransaction().commit();
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			if (session != null)
				session.getTransaction().rollback();
			throw ex;
		}
		return entityList;
	}

}
