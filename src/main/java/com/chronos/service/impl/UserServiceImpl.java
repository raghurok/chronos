package com.chronos.service.impl;

import com.chronos.domain.User;
import com.chronos.service.UserManager;

public class UserServiceImpl implements UserManager {
	
	private User user;

	@Override
	public void retrieveUser(String userName) {
		
		//TODO: Replace the following with DAO
		this.user = new User();
		user.setUserName(userName);
		user.setPassword("admin");
		
	}

	@Override
	public boolean authenticateUser() {
		
		if("admin".equals(user.getUserName()) && "admin".equals(user.getPassword())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isAdmin() {
		if("admin".equals(user.getUserName()) && "admin".equals(user.getPassword())) {
			return true;
		}
		return false;
	}
	
	

}
