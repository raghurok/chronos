package com.chronos.service;

import com.chronos.web.LoginForm;

public interface UserManager {	
	
	public void retrieveUser(String userName);
	public boolean authenticateUser();
	public boolean isAdmin();
	
}
