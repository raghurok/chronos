package com.chronos.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chronos.domain.User;
import com.chronos.service.impl.UserServiceImpl;
import com.chronos.web.LoginForm;

@Controller
@RequestMapping("login.html")
public class LoginController {
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView login() {
		return new ModelAndView("login","login",new LoginForm());
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String loginValidate(@ModelAttribute("login")LoginForm loginForm, ModelMap model) {
		
		UserServiceImpl userServiceImpl = new UserServiceImpl();
		userServiceImpl.retrieveUser(loginForm.getUsername());
		if(userServiceImpl.authenticateUser()) {
			if(userServiceImpl.isAdmin()) {
				System.out.println("admin is home");
				return "success";
			}
		}
		
		
		
		System.out.println(loginForm.getUsername()+"ddddd35345");
		System.out.println(loginForm.getPassword());
		return "login";
	}
}
