package com.chronos.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateSessionManager {
	private static SessionFactory sessionFactory = buildSessionFactory();
	private static ServiceRegistry serviceRegistry;
	public static int i = 0;

	private static SessionFactory buildSessionFactory() { 
		try{
			i++;
			System.out.println("-------------------------------------------------------" + i);
			Configuration configuration = new Configuration().configure();
			
			serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
			i++;
			System.out.println("-------------------------------------------------------" + i);
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			return sessionFactory;
		} catch(Exception ex){
			ex.printStackTrace();
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() throws ExceptionInInitializerError{
		if(sessionFactory == null)
			buildSessionFactory();
		return sessionFactory;
	}

	public static ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}
	
}
