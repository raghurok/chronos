package com.chronos.entity;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "RV_USER_DETAILS")
public class UserDetails  implements Serializable{

	@Id
	@Column(name = "USER_ID")
	private String userId;

	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@Column(name = "USER_ROLE")
	private String userRole;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "PRIMARY_EMAIL")
	private String pEmail;

	@Column(name = "SECONDARY_EMAIL")
	private String sEmail;

	@Column(name = "PHONE_NO")
	private String phoneNo;

	@Column(name = "LOCATION", nullable = false)
	private String location;
	
//	private List<UserDetails> userDetails;

//	@OneToOne(cascade = CascadeType.ALL, mappedBy = "userDetails")
//	private UserXRoles userXroles;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
				name = "RV_USER_X_ROLES", 
				joinColumns = {@JoinColumn(name = "USER_ID")}, 
				inverseJoinColumns = {@JoinColumn(name = "ROLE_ID")}
				)
	private List<Role> rolesList;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
				name = "RV_USER_X_TEAM", 
				joinColumns = {@JoinColumn(name = "USER_ID")}, 
				inverseJoinColumns = {@JoinColumn(name = "TEAM_ID")}
				)
	private List<Team> teamList = new ArrayList<Team>();
	
	@OneToMany(mappedBy = "userXprojectCpk.userDetails", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<UserXProject> userXprojectList = new ArrayList<UserXProject>();
	
	@OneToMany(mappedBy = "userDetails", fetch = FetchType.LAZY)
	private List<UserEfforts> userEffortsList = new ArrayList<UserEfforts>();
	
	public List<UserEfforts> getUserEffortsList() {
		return userEffortsList;
	}

	public void setUserEffortsList(List<UserEfforts> userEffortsList) {
		this.userEffortsList = userEffortsList;
	}

	public List<Team> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<Team> teamList) {
		this.teamList = teamList;
	}

	
	public List<UserXProject> getUserXprojectList() {
		return userXprojectList;
	}

	public void setUserXprojectList(List<UserXProject> userXprojectList) {
		this.userXprojectList = userXprojectList;
	}

	public String getUserId() {
		return userId;
	}

	public List<Role> getRolesList() {
		return rolesList;
	}

	public void setRolesList(List<Role> rolesList) {
		this.rolesList = rolesList;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	/*public UserXRoles getUserXroles() {
		return userXroles;
	}

	public void setUserXroles(UserXRoles userXroles) {
		this.userXroles = userXroles;
	}
*/
	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getpEmail() {
		return pEmail;
	}

	public void setpEmail(String pEmail) {
		this.pEmail = pEmail;
	}

	public String getsEmail() {
		return sEmail;
	}

	public void setsEmail(String sEmail) {
		this.sEmail = sEmail;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/*public List<UserDetails> getUserDetails() {
		return userDetails;
	}
	
	public void setUserDetails(List<UserDetails> userDetails) {
		this.userDetails = userDetails;
	}*/
}
