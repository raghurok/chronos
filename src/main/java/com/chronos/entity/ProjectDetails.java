package com.chronos.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "RV_PROJECT_DETAILS")
public class ProjectDetails implements Serializable{
	
	@Id
	@Column(name = "PROJECT_ID")
	private String projectId;
	
	@Column(name = "PROJECT_NAME")
	private String projectName;
	
	@Column(name = "LOE")
	private long loe;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "RELEASE_DATE")
	private Date releaseDate;
	
	@Column(name = "ACTIVE_FLAG")
	private String activeFlag;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "TEAM_ID", nullable = false)
	private Team team;
	
	@OneToMany(mappedBy = "userXprojectCpk.projectDetails", fetch = FetchType.LAZY)
	private List<UserXProject> userXprojectList = new ArrayList<UserXProject>();
	
	@OneToMany(mappedBy = "projectDetails", fetch = FetchType.LAZY)
	List<TaskXProject> taskXprojectList = new ArrayList<TaskXProject>();
	
	public List<TaskXProject> getTaskXprojectList() {
		return taskXprojectList;
	}

	public void setTaskXprojectList(List<TaskXProject> taskXprojectList) {
		this.taskXprojectList = taskXprojectList;
	}

	public List<UserXProject> getUserXprojectList() {
		return userXprojectList;
	}

	public void setUserXprojectList(List<UserXProject> userXprojectList) {
		this.userXprojectList = userXprojectList;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}
	
	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public long getLoe() {
		return loe;
	}

	public void setLoe(long loe) {
		this.loe = loe;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	
}
