package com.chronos.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "RV_USER_EFFORTS")
public class UserEfforts implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DAY", nullable = false, length = 10)
	private Date day;
	
	@Column(name = "EFFORT", nullable = false, precision = 22, scale = 0)
	private double effort;
	
	@Column(name = "UPDATION_DATE", nullable = false, length = 10)
	private Date updationDate;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "TASK_ID", nullable = false)
	private TaskXProject taskXproject;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID", nullable = false)
	private UserDetails userDetails;
	
	
	public TaskXProject getTaskXproject() {
		return taskXproject;
	}

	public void setTaskXproject(TaskXProject taskXproject) {
		this.taskXproject = taskXproject;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDay() {
		return this.day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public double getEffort() {
		return this.effort;
	}

	public void setEffort(double effort) {
		this.effort = effort;
	}

	public Date getUpdationDate() {
		return this.updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

}
