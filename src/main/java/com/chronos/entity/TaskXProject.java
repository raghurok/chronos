package com.chronos.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "RV_TASK_X_PROJECT")
public class TaskXProject {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TASK_ID")
	private long taskId;
	
	@Column(name = "TASK_NAME", nullable = false)
	private String taskName;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "PROJECT_ID", nullable = false)
	private ProjectDetails projectDetails;

	@OneToMany(mappedBy = "taskXproject", fetch = FetchType.LAZY)
	private List<UserEfforts> userEfforts = new ArrayList<UserEfforts>();
	
	public List<UserEfforts> getUserEfforts() {
		return userEfforts;
	}

	public void setUserEfforts(List<UserEfforts> userEfforts) {
		this.userEfforts = userEfforts;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public ProjectDetails getProjectDetails() {
		return projectDetails;
	}

	public void setProjectDetails(ProjectDetails projectDetails) {
		this.projectDetails = projectDetails;
	}
	
}
