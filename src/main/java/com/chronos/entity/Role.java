package com.chronos.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "RV_ROLES")
public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ROLE_ID")
	private long roleId;
	
	@Column(name = "ROLE_NAME")
	private String roleName;
	
//	@OneToOne(cascade = CascadeType.ALL, mappedBy = "roles")
//	private UserXRoles userXroles;

//	@OneToOne(cascade = CascadeType.ALL, targetEntity = UserXRoles.class)
//	@JoinColumn(name = "ROLE_ID")
//	private UserXRoles userXroles;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "RV_USER_X_ROLES", 
			joinColumns = {@JoinColumn(name = "ROLE_ID")}, 
			inverseJoinColumns = {@JoinColumn(name = "USER_ID")}
			)
	private List<UserDetails> userDetails = new ArrayList<UserDetails>();
	
	public List<UserDetails> getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(List<UserDetails> userDetails) {
		this.userDetails = userDetails;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
/*
	public UserXRoles getUserXroles() {
		return userXroles;
	}

	public void setUserXroles(UserXRoles userXroles) {
		this.userXroles = userXroles;
	}*/

	
}
