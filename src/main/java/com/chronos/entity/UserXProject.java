package com.chronos.entity;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "RV_USER_X_PROJECT")
@AssociationOverrides({
	@AssociationOverride(name = "userXprojectCpk.userDetails", joinColumns = @JoinColumn(name = "USER_ID")), 
	@AssociationOverride(name = "userXprojectCpk.projectDetails", joinColumns = @JoinColumn(name= "PROJECT_ID"))
})
public class UserXProject implements Serializable{
	
	@EmbeddedId
	private UserXProjectCpk userXprojectCpk = new UserXProjectCpk();
	
	@Column(name = "PROJECT_ROLE")
	private String projectRole;
	/*
	@Transient
	private UserDetails userDetails = new UserDetails();
	
	@Transient
	private ProjectDetails projectDetails = new ProjectDetails();
*/
	public UserXProjectCpk getUserXprojectCpk() {
		return userXprojectCpk;
	}

	public void setUserXprojectCpk(UserXProjectCpk userXprojectCpk) {
		this.userXprojectCpk = userXprojectCpk;
	}

	public String getProjectRole() {
		return projectRole;
	}

	public void setProjectRole(String projectRole) {
		this.projectRole = projectRole;
	}
	
	@Transient
	public UserDetails getUserDetails() {
		return getUserXprojectCpk().getUserDetails();
	}

	public void setUserDetails(UserDetails userDetails) {
		getUserXprojectCpk().setUserDetails(userDetails);
	}

	@Transient
	public ProjectDetails getProjectDetails() {
		return getUserXprojectCpk().getProjectDetails();
	}

	public void setProjectDetails(ProjectDetails projectDetails) {
		getUserXprojectCpk().setProjectDetails(projectDetails);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((projectRole == null) ? 0 : projectRole.hashCode());
		result = prime * result
				+ ((userXprojectCpk == null) ? 0 : userXprojectCpk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserXProject other = (UserXProject) obj;
		if (projectRole == null) {
			if (other.projectRole != null)
				return false;
		} else if (!projectRole.equals(other.projectRole))
			return false;
		if (userXprojectCpk == null) {
			if (other.userXprojectCpk != null)
				return false;
		} else if (!userXprojectCpk.equals(other.userXprojectCpk))
			return false;
		return true;
	}

	
}
