package com.chronos.entity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;

import com.chronos.dao.impl.ProjectDetailsDAOImpl;
import com.chronos.dao.impl.UserDetailsDAOImpl;
import com.chronos.dao.impl.UserXProjectDAOImpl;
import com.chronos.util.HibernateSessionManager;

/**
 * Servlet implementation class TestHibernate
 */
@WebServlet("/TestHibernate")
public class TestHibernate extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TestHibernate.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestHibernate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
//			SessionFactory sFactory  = HibernateSessionManager.getSessionFactory();
			
		
//		Session session = sFactory.getCurrentSession();
//		Transaction txn = session.beginTransaction();
		UserDetailsDAOImpl userDetailsImpl = new UserDetailsDAOImpl();
//		userDetailsImpl.setSession(session);
		UserDetails user = userDetailsImpl.findByPk("A05");
		ProjectDetails projectDetails = new ProjectDetailsDAOImpl().findByPk("PR05");
		UserXProjectDAOImpl userXProjectImpl = new UserXProjectDAOImpl();
		UserXProjectCpk userx = new UserXProjectCpk();
		userx.setProjectDetails(projectDetails);
		userx.setUserDetails(user);
		
		
		
		UserXProject userP= userXProjectImpl.getDetailsByUserIdProjectIdCPkey(userx);
		
		
//		UserXProject userxp = new UserXProject();
//		userxp.setProjectDetails(projectDetails);
//		userxp.setUserDetails(user);
//		userXProjectImpl.insert(userxp);
		
//		UserDetails userDetails = new UserDetails();
//		userDetails.setFirstName("Vishwas");
//		userDetails.setLastName("Nayak");
//		userDetails.setLocation("Offshore");
//		userDetails.setPassword("password");
//		userDetails.setpEmail("pEmail");
//		userDetails.setsEmail("sEmail");
//		userDetails.setUserId("A088");
//		userDetails.setUserRole("userRole");
//		userDetailsImpl.insert(userDetails);
//		userDetailsImpl.commitTransaction();
		
//		UserDetails user = (UserDetails) session.get(UserDetails.class, "A06");
//		txn.commit();
////		session.beginTransaction();
//		user = (UserDetails) session.get(UserDetails.class, "A05");
//		txn.commit();
/*		UserDetails userDetails = new UserDetails();
		userDetails.setFirstName("Vishwas");
		userDetails.setLastName("Nayak");
		userDetails.setLocation("Offshore");
		userDetails.setPassword("password");
		userDetails.setpEmail("pEmail");
		userDetails.setsEmail("sEmail");
		userDetails.setUserId("A07");
		userDetails.setUserRole("userRole");
		session.save(userDetails);
		
		Team team = new Team();
		team.setTeamName("Sigma1a");
		team.getUserDetailsList().add(userDetails);
		session.save(team);

		ProjectDetails projectDetails = new ProjectDetails();
		projectDetails.setProjectId("PR06");
		projectDetails.setProjectName("project");
		projectDetails.setReleaseDate(new Date());
		projectDetails.setLoe(201);
		projectDetails.setActiveFlag("y");
		projectDetails.setTeam(team);
		session.save(projectDetails);
		
		UserXProject userXProject = new UserXProject();
		userXProject.setProjectDetails(projectDetails);
		userXProject.setUserDetails(userDetails);
		userXProject.setProjectRole("projectRole");
		session.save(userXProject);
		
		TaskXProject taskXProject = new TaskXProject();
		taskXProject.setTaskName("taskName1");;
		taskXProject.setProjectDetails(projectDetails);
		session.save(taskXProject);
		
		UserEfforts userEfforts = new UserEfforts();
		userEfforts.setDay(new Date());
		userEfforts.setEffort(5.25);
		userEfforts.setUpdationDate(new Date());
		userEfforts.setUserDetails(userDetails);
		userEfforts.setTaskXproject(taskXProject);
		session.save(userEfforts);

//		UserXRoles userXroles = new UserXRoles();
//		UserRoleCpk userRoleCpk = new UserRoleCpk();
//		userRoleCpk.setRoleId(121);
//		userRoleCpk.setUserId("a031");
//		userXroles.setUserRoleCpk(userRoleCpk);
		*/
		
//		txn.commit();
		String a ="Hello";
		PrintWriter out = response.getWriter();
		log.info("Testing");
		out.println("<html>");
		out.println("<body>");
		out.println("<h1>" + userP.getProjectRole() + "</h1>");
		out.println("</body>");
		out.println("</html>");
//		session.close();
		} catch (Throwable ex) {
			
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		/*
		 Query query = session.createQuery("from Department where DEPT_ID =:deptId");
		long num = 111112;
		query.setParameter("deptId", num);
		
		List<Department> values = query.list();
			
		log.info("-------------------------------");
		Department dept = values.get(0);
		log.info("DeptID = " + dept.getDeptID());
		log.info("Dept name = " + dept.getDeptName());
		log.info("-------------------------------");
		*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
