package com.chronos.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class UserXProjectCpk implements Serializable{
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private UserDetails userDetails;
	
	@ManyToOne
	@JoinColumn(name = "PROJECT_ID")
	private ProjectDetails projectDetails;
	
	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public ProjectDetails getProjectDetails() {
		return projectDetails;
	}

	public void setProjectDetails(ProjectDetails projectDetails) {
		this.projectDetails = projectDetails;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((projectDetails == null) ? 0 : projectDetails.hashCode());
		result = prime * result
				+ ((userDetails == null) ? 0 : userDetails.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserXProjectCpk other = (UserXProjectCpk) obj;
		if (projectDetails == null) {
			if (other.projectDetails != null)
				return false;
		} else if (!projectDetails.equals(other.projectDetails))
			return false;
		if (userDetails == null) {
			if (other.userDetails != null)
				return false;
		} else if (!userDetails.equals(other.userDetails))
			return false;
		return true;
	}

}
