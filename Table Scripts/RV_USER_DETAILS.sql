CREATE TABLE RV_USER_DETAILS (
USER_ID VARCHAR(20), 
PASSWORD VARCHAR(20) NOT NULL, 
USER_ROLE VARCHAR(20), 
FIRST_NAME VARCHAR(50), 
LAST_NAME VARCHAR(50), 
PRIMARY_EMAIL VARCHAR(50), 
SECONDARY_EMAIL VARCHAR(50), 
PHONE_NO VARCHAR (15), 
LOCATION VARCHAR(20) NOT NULL, 
PRIMARY KEY (USER_ID)
);