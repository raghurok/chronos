CREATE TABLE RV_USER_EFFORTS (
ID INTEGER UNSIGNED NOT NULL AUTO_INCREMENT, 
DAY DATE NOT NULL, 
TASK_ID INTEGER(20) NOT NULL REFERENCES RV_PROJECT_X_TASK(TASK_ID), 
USER_ID VARCHAR(20) NOT NULL REFERENCES RV_USER_DETAILS(USER_ID),
EFFORT DOUBLE NOT NULL DEFAULT 0,
UPDATION_DATE DATE NOT NULL,
PRIMARY KEY(ID)
);