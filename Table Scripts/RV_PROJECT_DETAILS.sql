CREATE TABLE RV_PROJECT_DETAILS (
PROJECT_ID VARCHAR(20), PROJECT_NAME VARCHAR(100), 
LOE INTEGER(5), 
TEAM_ID INTEGER(20) NOT NULL REFERENCES RV_TEAM (TEAM_ID), 
RELEASE_DATE DATE, 
ACTIVE_FLAG VARCHAR(1), 
PRIMARY KEY (PROJECT_ID)
);